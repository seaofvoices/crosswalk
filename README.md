[![pipeline status](https://gitlab.com/seaofvoices/crosswalk/badges/master/pipeline.svg)](https://gitlab.com/seaofvoices/crosswalk/commits/master)
[![license](https://img.shields.io/badge/license-MIT-blue)](LICENSE.txt)

# crosswalk
This framework wraps the remote events and remote functions calls in order to speed up development.

# [Documentation](https://seaofvoices.gitlab.io/crosswalk/)

Check out the guide and quick reference on the [documentation site](https://seaofvoices.gitlab.io/crosswalk/).

# License

crosswalk is available under the MIT license. See [LICENSE.txt](LICENSE.txt) for details.
