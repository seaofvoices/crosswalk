mkdocs==1.1
mkdocs-material==5.2.0
mkdocs-minify-plugin==0.3.0
mkdocs-awesome-pages-plugin==2.2.1
pymdown-extensions==7.1
